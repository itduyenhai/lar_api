<?php namespace Module\Auth\Http\Controllers;

use App\Http\Controllers\Controller;
use Module\Auth\Http\Request;
use User;
use Brcypt;

class AuthController extends Controller
{
    protected $module = 'auth';

    // Sign Up
    public function signup(Request $request)
    {
      $user = new User();
      $user->name = $request->name;
      $user->email = $request->email;
      $user->password = Bcrypt($request->password);
      $user->save();

      return response()->json(['user' => $user], 200);
    }

    // Sign In
    public function signin(Request $request)
    {

    }
}
